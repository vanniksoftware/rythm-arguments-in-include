package controllers;

import java.util.ArrayList;
import java.util.List;

import models.dto.ProjectDto;
import play.mvc.Controller;

public class Application extends Controller {

    public static void index() {
    	List<ProjectDto> myProjects = new ArrayList<ProjectDto>();
    	ProjectDto dtoOne = new ProjectDto();
    	dtoOne.name = "One";
    	myProjects.add(dtoOne);
    	ProjectDto dtoTwo = new ProjectDto();
    	dtoTwo.name = "Two";
    	myProjects.add(dtoTwo);
        render(myProjects);
    }

}